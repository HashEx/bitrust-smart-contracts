# Bitrust smart contracts repository

### Setting up ###

* Clone repository
* run ```npm install```
* run `npm install -g solium` to install solium linter globally

### Using template ###

* To run tests run `truffle test` or `npm test` in console
* To run coverage report run `npm run coverage`
* to run linter `npm run lint`