pragma solidity ^0.4.18;

import "zeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "zeppelin-solidity/contracts/ownership/Ownable.sol";

contract BitrustToken is StandardToken, Ownable {

	string public name = "BITRUST Token";
	string public symbol = "BTF";
	uint8 public decimals = 18;
	bool icoFinished = false;

	address teamTimelockAddress;
	address advisorsAddress;
	address reserveTimeLockAddress;

	uint256 public constant INITIAL_SUPPLY = 607000000 * (10 ** uint256(decimals));

	/**
	* @dev Token constructor that sets intial values and gives initial supply to msg.sender
	*/
	function BitrustToken(address _teamTimelockAddress, 
		address _advisorsAddress, address _reserveTimeLockAddress) public {
		require(_teamTimelockAddress != address(0));
		require(_advisorsAddress != address(0));
		require(_reserveTimeLockAddress != address(0));
		teamTimelockAddress = _teamTimelockAddress;
		advisorsAddress = _advisorsAddress;
		reserveTimeLockAddress = _reserveTimeLockAddress;
	}

	function issueTokens(uint256 tokensSold) external onlyOwner {
		require(!icoFinished);
		require(tokensSold <= uint256(455250000).mul(10 ** uint256(decimals)));
		require(tokensSold > 0);

		uint256 teamBonuses = INITIAL_SUPPLY.mul(15).div(100);
		balances[teamTimelockAddress] = teamBonuses;

		uint256 advisorsTokens = INITIAL_SUPPLY.mul(7).div(100);
		balances[advisorsAddress] = advisorsTokens;

		uint256 reserveTokens = INITIAL_SUPPLY.mul(10).div(100);
		balances[reserveTimeLockAddress] = reserveTokens;

		balances[msg.sender] = tokensSold;

		totalSupply_ = tokensSold.add(teamBonuses).add(advisorsTokens).add(reserveTokens);
		icoFinished = true;
	}

}