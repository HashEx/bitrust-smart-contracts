const BitrustToken = artifacts.require('./BitrustToken')

import EVMThrow from './helpers/EVMThrow'
const BigNumber = web3.BigNumber
const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should()

const overallTokens = 607000000;

contract('BitrustToken', accounts => {

	beforeEach(async function() {
		this.owner = accounts[0]
		this.token = await BitrustToken.new(
			accounts[7],
			accounts[8],
			accounts[9])
	})


	it('should set zero balance to owner on creation', async function() {
		const balance = await this.token.balanceOf.call(this.owner)
		assert.equal(balance.valueOf(), 0, "owner should have zero balance")
	})

	it('should not allow to run issue tokens twice', async function() {
		await this.token.issueTokens(100)
		await this.token.issueTokens(100).should.be.rejected
	})

	it('should set owner balance', async function() {
		await this.token.issueTokens(1000)

		const ownerBalance = await this.token.balanceOf.call(this.owner)
		assert.equal(ownerBalance.valueOf(), 1000, "owner balance should be set")
	})

	it('should set team bonuses', async function() {
		await this.token.issueTokens(1000)

		const expectedBalance = new BigNumber('91050000000000000000000000')

		const teamBalance = await this.token.balanceOf.call(accounts[7])

		expectedBalance.should.be.bignumber.equal(teamBalance)
	})

	it('should set reserve bonuses', async function() {
		await this.token.issueTokens(1000)

		const reserveBalance = await this.token.balanceOf.call(accounts[9])
		const expectedBalance = new BigNumber('60700000000000000000000000')
		expectedBalance.should.be.bignumber.equal(reserveBalance)
	})

	it('should set advisors bonuses', async function() {
		await this.token.issueTokens(1000)

		const advisorsBalance = await this.token.balanceOf.call(accounts[8])
		const expectedBalance = new BigNumber('42490000000000000000000000')
		expectedBalance.should.be.bignumber.equal(advisorsBalance)
	})

	it('should set total supply', async function() {
		const teamBalance = new BigNumber('91050000000000000000000000')
		const reserveBalance = new BigNumber('60700000000000000000000000')
		const advisorsBalance = new BigNumber('42490000000000000000000000')
		const boughtTokens = new BigNumber('364200000000000000000000000') 

		await this.token.issueTokens(boughtTokens)

		const sum = teamBalance.plus(reserveBalance)
									.plus(advisorsBalance)
									.plus(boughtTokens)
		const totalSupply = await this.token.totalSupply.call()
		sum.should.be.bignumber.equal(totalSupply)
	})

	it('should not allow to run issue with zero bought tokens', async function() {
		await this.token.issueTokens(0).should.be.rejected
	})

	it('should not allow to run issue tokens function with param more than max ' +
		'available sold and bonus tokens', async function() {

		const maxBought = new BigNumber('364200000000000000000000000') 
		const maxBonus = new BigNumber('91050000000000000000000000') //15%

		const exceedAmount = maxBought.plus(maxBonus).plus(1)

		await this.token.issueTokens(exceedAmount).should.be.rejected
	})

	it('should allow to run issue tokesn with max value', async function() {
		const maxBought = new BigNumber('364200000000000000000000000') 
		const maxBonus = new BigNumber('91050000000000000000000000') //15%

		const exceedAmount = maxBought.plus(maxBonus)

		await this.token.issueTokens(exceedAmount)
		//should not fail
	})

})